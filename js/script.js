const form = document.forms[0];
let dataList = [];

form.addEventListener("submit", (e) => {

    e.preventDefault();
    const btn = document.querySelector("#btnSubmit");
    const form = document.querySelector("#form");
    const nome = form.querySelector("#nome").value;
    const cpf = form.querySelector("#cpf").value;
    const rua = form.querySelector("#rua").value;
    const numero = form.querySelector("#numero").value;
    const bairro = form.querySelector("#bairro").value;
    const dataNascimento = form.querySelector("#dataNascimento").value;
    const rendaText = form.querySelector("#renda").value;
    const renda = parseFloat(rendaText);
    const profissao = form.querySelector("#profissao").value;

    btn.innerText = "Enviado!";

    const endereco = {
        rua,
        numero,
        bairro
    }

    if (nome.length > 100) {
        alert("Digite o nome novamente abreviado, nome muito grande!");
    }

    if (rua.length > 100) {
        alert("Digite a rua novamente abreviada, nome muito grande!");
    }

    if (cpf.length > 11) {
        alert("Digite a o cpf novamente, o cpf digitado esta incorreto!");
    }

    if (renda > 2787) {
        alert("Salário acima da média mensal dos brasileiros.");
    } else {
        alert("Salário abaixo da média mensal dos brasileiros.");
    }

    if (!(nome.length > 100) && !(rua.length > 100) && !(cpf.length > 11)) {

        console.log("Entrou")
        const dataForm = {
            nome,
            cpf,
            endereco,
            dataNascimento,
            renda,
            profissao
        }

        mostraDados(dataForm);

        dataList.push(dataForm);
    }

    setTimeout(() => {
        btn.innerText = "Enviar";
    }, 2000);

});

function mostraDados(dados) {
    const boxResultado = document.querySelector("#resultado-text")
    boxResultado.innerHTML = `
        <p><b>Nome:</b> ${dados.nome}</p>
        <p><b>CPF:</b> ${dados.cpf}</p>
        <p><b>Rua:</b> ${dados.endereco.rua}</p>
        <p><b>Numero:</b> ${dados.endereco.numero}</p>
        <p><b>Bairro:</b> ${dados.endereco.bairro}</p>
        <p><b>Data Nascimento:</b> ${dados.dataNascimento}</p>
        <p><b>Renda:</b> R$ ${dados.renda},00</p>
        <p><b>Profissão:</b> ${dados.profissao}</p>
    `;
}


